---
title: Explorers
description: Grey Software's Open Source Explorers
category: Team
postion: 13
explorers:  
  - name: Jia Hong
    avatar: https://avatars.githubusercontent.com/u/47465704?s=400&u=9aa7cfe98ec20f7b33abf59e4a7dd1fa92b74b8f
    position: Explorer
    github: https://github.com/jiahongle
    gitlab: https://gitlab.com/jiahongle
    linkedin: https://linkedin.com/in/jiahongle
  - name: Agnes Lin
    avatar: https://avatars3.githubusercontent.com/u/50331796?v=3&s=200
    position: Explorer
    github: https://github.com/agnes512
    gitlab: https://gitlab.com/agnes512
    linkedin: https://www.linkedin.com/in/agnes-yuchi-lin-a4601b172/
  - name: Kunal Verma
    avatar: https://avatars.githubusercontent.com/u/72245772?s=460&u=756170512bf927ef4410c0d94f3b630510e910cb&v=4
    position: Explorer
    github: https://github.com/Zipher007
    gitlab: https://gitlab.com/Zipher007
    linkedin: https://www.linkedin.com/in/verma-kunal/
  - name: Deepanshu Dhruw
    avatar: https://avatars.githubusercontent.com/u/55559527?s=400&u=7a06beafb54ec1d480eed23bfc3dc91a9a49bd3d
    position: Explorer
    github: https://github.com/devblin
    gitlab: https://gitlab.com/devblin
    linkedin: https://www.linkedin.com/in/devblin/
---

This page is for the citizens of the internet that wanted to explore the world of open source and did so by completing our [Onboarding Process](https://onboarding.grey.software)

<team-profiles :profiles="explorers"></team-profiles>
