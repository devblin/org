---
title: Feedback 💬
description: Grey Software's Organization Website
category: Info
position: 5
---

## Feedback from the Open Source World

If you want to learn to code, go checkout Grey Software! They help you contribute to OSS and learn while doing it. - Matic Zavadlal, Creator of SwiftGraphQL

## General Feedback from students

Even though tons of youtube videos are available, it’s a different experience to have someone that understands what you are going through to guide you in the project. - Shawn, Student at UTM

I have been programming on the side for the past year now, mainly using online resources to pair program... yet one session with my mentor was more enriching than hours spent on these side tutorials. - Osama, Grey Software Explorer

I got to experience what it was like being onboarded into a codebase and collaborating with a skilled developer and designer. I got the software development education I was looking for." - Milind, Grey Software Apprentice

## Feedback on our Open Source Apprenticeship Program

### Structure and Mentorship

Having a leader that has built several successful projects allows for a cleaner road-map, thus preventing premature production. - Shawn, Student at UTM

It was nice to have a mentor and professor to bounce ideas off of, and build within a structure. Building in a way that's scalable in a team is hard, and I'm glad to have given it a try. - Arnav, Student at UTM

Arsala did a great job as a mentor since he understands the challenge that a 2nd year student could be facing. He also understands the situation that most of his students have no experience with software development as a team before. His constant advice and critiques helps the team to steadily move toward the right direction and for students to grow. - Brian, Student at UTM

### Industry workflow

This course simulated what it will be like to work in the industry by encouraging the students to work asynchronously and proactively rather than following the instructions of an assignment. - Shawn, Student at UTM

I had to get used to a different mindset; working asynchronously and making progress daily instead of cramming before a deadline. - Lee, Student at UTM

The most useful was the overall workflow, because it can be applied to any future projects. All projects need proper documentation and remote repo, and group members need to work asynchronously by managing branches, pull requests, issues and milestones. - Baichen, Student at UTM

## Feedback from Osama, 6 months after apprenticing

_Osama was an apprentice with Grey software during the summer of 2020, Where he contributed to a plethora of projects passionately learned tools and technical knowledge related to modern web-apps and devops_

Reflecting on my work a few months later, I have had a massive technical boost being part of Grey Software throughout the summer. Besides receiving valuable mentorship and working on meaningful projects, I was also exposed to a highly professional workflow backed by Arsala's expertise and dedication to the organization.

I can confidently say that my time at Grey Software has enabled me to become more thorough in the technical work I do and motivated to tackle more complex technical challenges with confidence in my abilities. As a result of the experience I've gained, I have managed to go from a summer with only a single interview during my 2nd year to interviewing at some great companies during my 3rd year, including Amazon, BlackBerry, ProofPoint, and Telus.

Open Source work is a truly enriching prospect, going beyond the grind of academic work. In addition to the technical skills it provides, you also gain valuable insight into some of the great ideas people out there have had and get to contribute to them. At Grey Software, I was delighted by the approach to immerse, educate, and reflect upon best practices and my professional development :)
